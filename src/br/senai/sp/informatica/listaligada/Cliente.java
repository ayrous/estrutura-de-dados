package br.senai.sp.informatica.listaligada;


public class Cliente {
	
	private String nome;
	private String endereco;
	private String dataNascimento;
	
	//gettersAndSetters
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getEndereco() {
		return endereco;
	}
	
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public String getDataNascimento() {
		return dataNascimento;
	}
	
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Cliente) {

			//converte o obj para Produto
			Cliente outroCliente = (Cliente) obj;
			if(this.nome.equals(outroCliente.getNome()) && this.endereco.equals (outroCliente.getEndereco()) && this.dataNascimento.equals(outroCliente.getDataNascimento())) {
				return true;
			}else {
				return false;
			}
			
		} else {
			// Se o obj n�o pertencer a classe Produto
			return false;
		}
	}
	
	@Override
	public String toString() {
		return getNome() + "\n" + getEndereco() + "\n " + getDataNascimento() +"\n";
	}
	
}