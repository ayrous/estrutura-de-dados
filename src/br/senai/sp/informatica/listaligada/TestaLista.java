package br.senai.sp.informatica.listaligada;

import java.util.LinkedList;

import javax.swing.JOptionPane;

/**
 * Programa que simula um cadastro de clientes
 * 
 * @author �ris Campanella Cabral
 *
 */
public class TestaLista {

	public static void main(String[] args) {

		Cliente cliente = new Cliente();
		LinkedList<Cliente> clientes = new LinkedList<>();
		int confirm;

		do {
			JOptionPane.showMessageDialog(null, "Bem vindo!", "Simulador de Cadastro", JOptionPane.INFORMATION_MESSAGE);
			String opcao = JOptionPane.showInputDialog(null,
					"Escolha uma op��o: \n 1) Listar os clientes\n 2)Cadastrar um cliente\n3)Alterar um cliente\n4)Deletar um cliente",
					"Op��es", JOptionPane.INFORMATION_MESSAGE);
			int opcoes = Integer.parseInt(opcao);

			if (opcoes == 1) {

				if (clientes.isEmpty()) {
					JOptionPane.showMessageDialog(null, "A lista est� vazia!", "Oops...", JOptionPane.WARNING_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, clientes.toArray(), "Clientes",
							JOptionPane.INFORMATION_MESSAGE);

				}
			} else if (opcoes == 2) {
				Cliente novo = new Cliente();
				String nome = JOptionPane.showInputDialog(null, "Coloque o Nome: ", "Cadastro",
						JOptionPane.QUESTION_MESSAGE);
				novo.setNome(nome);
				String enderec = JOptionPane.showInputDialog(null, "Coloque o endere�o:", "Cadastro",
						JOptionPane.QUESTION_MESSAGE);
				novo.setEndereco(enderec);
				String data = JOptionPane.showInputDialog(null, "Sua data de nascimento: ", "Cadastro",
						JOptionPane.QUESTION_MESSAGE);
				novo.setDataNascimento(data);
				cliente = novo;
				clientes.add(cliente);
				JOptionPane.showMessageDialog(null, clientes.peekLast(), "Adicionado!",
						JOptionPane.INFORMATION_MESSAGE);

			} else if (opcoes == 3) {
				if (clientes.isEmpty()) {
					JOptionPane.showMessageDialog(null, "A lista est� vazia!", "Oops...", JOptionPane.WARNING_MESSAGE);
				} else {

					String indice = JOptionPane.showInputDialog(null, "Informe o �ndice do cliente: ", "Atualizando...",
							JOptionPane.QUESTION_MESSAGE);
					int ind = Integer.parseInt(indice);

					JOptionPane.showMessageDialog(null, clientes.get(ind));
					String nome = JOptionPane.showInputDialog(null, "Coloque o Nome:", "Atualizando...",
							JOptionPane.QUESTION_MESSAGE);
					clientes.get(ind).setNome(nome);
					String enderec = JOptionPane.showInputDialog(null, "Coloque o endere�o:", "Atualizando...",
							JOptionPane.QUESTION_MESSAGE);
					clientes.get(ind).setEndereco(enderec);
					String data = JOptionPane.showInputDialog(null, "Sua data de nascimento: ", "Atualizando...",
							JOptionPane.QUESTION_MESSAGE);
					clientes.get(ind).setDataNascimento(data);
					// clientes.add(ind, cliente);
					JOptionPane.showMessageDialog(null, clientes.get(ind), "Atualizado!",
							JOptionPane.INFORMATION_MESSAGE);
				}
			} else if (opcoes == 4) {
				if (clientes.isEmpty()) {
					JOptionPane.showMessageDialog(null, "A lista est� vazia!", "Oops...", JOptionPane.WARNING_MESSAGE);
				} else {

					String del = JOptionPane.showInputDialog(null, "Informe o indice do cliente: ", "Deletando...",
							JOptionPane.WARNING_MESSAGE);
					int deletar = Integer.parseInt(del);
					JOptionPane.showMessageDialog(null, "Foi �timo t�-lo conosco, " + clientes.get(deletar).getNome(),
							"Deletado..", JOptionPane.INFORMATION_MESSAGE);
					clientes.remove(deletar);
					if (clientes.isEmpty()) {
						JOptionPane.showMessageDialog(null, "A lista est� vazia!", "Oops...", JOptionPane.WARNING_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, clientes.toArray(), "Atualizado!",
								JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}
			String confirmar = JOptionPane.showInputDialog(null, "Insira 1 pra SAIR", "Encerrando...",
					JOptionPane.CLOSED_OPTION);
			confirm = Integer.parseInt(confirmar);
		} while (confirm != 1);

	}
}