package br.senai.sp.informatica.pilha;

import java.util.Stack;

import javax.swing.JOptionPane;

/**
 * Programa que simula os bot�es de
 * avan�ar e retornar do navegador
 * @author �ris Campanella Cabral
 *
 */
public class TestaPilha {

	public static void main(String[] args) {

		Stack<String> pilhaAtual = new Stack<>();
		Stack<String> voltar = new Stack<>();
		Stack<String> avancar = new Stack<>();
		int confirmacao;

		do {
//			if (pilhaAtual.isEmpty()) {
				String add = JOptionPane.showInputDialog(null, "Insira uma URL: ", "Simula��o dos Bot�es da Web", JOptionPane.INFORMATION_MESSAGE);
				pilhaAtual.push(add);
			
	//		} else {
					JOptionPane.showMessageDialog(null, "Bem vindo!\n" + pilhaAtual.peek(), "Simula��o dos Bot�es da Web", JOptionPane.INFORMATION_MESSAGE);
		//	}
				String msg = JOptionPane.showInputDialog("1)Anterior  " + "2)Pr�ximo  " + "3)Sair");

			int navegar = Integer.parseInt(msg);

			if (navegar == 1) {
			
				if (voltar.isEmpty()) {
					JOptionPane.showMessageDialog(null, "N�o h� nada no hist�rico!", "Oops..", JOptionPane.WARNING_MESSAGE);
					
				} else {
					JOptionPane.showMessageDialog(null,  voltar.peek(), "Anterior", JOptionPane.INFORMATION_MESSAGE);
					avancar.push( pilhaAtual.peek());
					pilhaAtual.push(voltar.peek());
					voltar.pop();
					
				}
			}
			if (navegar == 2) {
				// se for para o proximo, o proximo vira atual e o atual vira anterior
				if (avancar.isEmpty()) {
					JOptionPane.showMessageDialog(null, "N�oo h� nada no hist�rico!", "Oops...",
							JOptionPane.WARNING_MESSAGE);					
				} else {
					JOptionPane.showMessageDialog(null, avancar.peek(), "Pr�xima P�gina",
							JOptionPane.INFORMATION_MESSAGE);
					voltar.push( pilhaAtual.peek());
					pilhaAtual.push(avancar.peek());
					avancar.pop();
					
				}
			}
			confirmacao = navegar;
		if(confirmacao != 3) {
			voltar.push(pilhaAtual.peek());
		//	pilhaAtual = avancar;
		}else {
			break;
		}
		}while(confirmacao != 3);
				//voltar = pilhaAtual;
	}
}