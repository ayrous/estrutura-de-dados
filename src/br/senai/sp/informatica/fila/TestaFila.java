package br.senai.sp.informatica.fila;

//import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

import javax.swing.JOptionPane;

/**
 * Programa b�sico que simula uma fila de clientes 
 * e filas nos respectivos caixas
 * @author �ris Campanella Cabral
 *
 */

public class TestaFila {

	public static void main(String[] args) {
		int confirmacao;
		//	String nome;
		Queue<String> filaClientes = new PriorityQueue<>();
		Queue<String> filaUm = new PriorityQueue<>();
		Queue<String> filaDois = new PriorityQueue<>();
		Queue<String> filaTres = new PriorityQueue<>();

	/*	LinkedList<String> caixaUm = new LinkedList<>();
		LinkedList<String> caixaDois = new LinkedList<>();
		LinkedList<String> caixaTres = new LinkedList<>();*/

		do {
			JOptionPane.showMessageDialog(null, "Bem-vindo!", "Simula��o de Caixas", JOptionPane.INFORMATION_MESSAGE);
			String escolha = JOptionPane.showInputDialog(null, "Op��es:\n\n"
					+ "1)Adicionar clientes � fila\n 2)Colocar um cliente no caixa\n 3)Remover um cliente do caixa");
			int opcao = Integer.parseInt(escolha);

			if (opcao == 1) {
				String name = JOptionPane.showInputDialog(null, "Insira o nome do cliente: ", "Adicionando...",
						JOptionPane.QUESTION_MESSAGE);
				filaClientes.add(name);
				JOptionPane.showMessageDialog(null, filaClientes.toArray(), "Fila de Clientes",
						JOptionPane.INFORMATION_MESSAGE);
			}

			else if (opcao == 2) {

				if (filaClientes.isEmpty()) {
					JOptionPane.showMessageDialog(null, "N�o h� clientes..", "Oops..", JOptionPane.WARNING_MESSAGE);
				} else {

					if (!filaUm.isEmpty() & !filaDois.isEmpty() & !filaTres.isEmpty()) {
						if (filaUm.size() == filaDois.size()) {
							filaUm.add(filaClientes.peek());
							filaClientes.poll();
							JOptionPane.showMessageDialog(null, filaUm.toArray(), "Fila Caixa 1",
									JOptionPane.INFORMATION_MESSAGE);

						}else if(filaUm.size() == filaTres.size()) {
							filaUm.add(filaClientes.peek());
							filaClientes.poll();
							JOptionPane.showMessageDialog(null, filaUm.toArray(), "Fila Caixa 1",
									JOptionPane.INFORMATION_MESSAGE);
							
						}else if(filaDois.size() == filaTres.size()) {
							filaDois.add(filaClientes.peek());
							filaClientes.poll();
							JOptionPane.showMessageDialog(null, filaDois.toArray(), "Fila Caixa 2",
									JOptionPane.INFORMATION_MESSAGE);
					
						}else if (filaUm.size() > filaDois.size() & filaUm.size() > filaTres.size()) {
							if (filaDois.size() > filaTres.size()) {
								filaTres.add(filaClientes.peek());
								filaClientes.poll();
								JOptionPane.showMessageDialog(null, filaTres.toArray(), "Fila Caixa 3",
										JOptionPane.INFORMATION_MESSAGE);

							} else if (filaTres.size() > filaDois.size()) {
								filaDois.add(filaClientes.peek());
								filaClientes.poll();
								JOptionPane.showMessageDialog(null, filaDois.toArray(), "Fila Caixa 2",
										JOptionPane.INFORMATION_MESSAGE);

							}
						}
					} else if (filaDois.size() > filaUm.size() & filaDois.size() > filaTres.size()) {
						if (filaUm.size() > filaTres.size()) {
							filaTres.add(filaClientes.peek());
							filaClientes.poll();
							JOptionPane.showMessageDialog(null, filaTres.toArray(), "Fila Caixa 3",
									JOptionPane.INFORMATION_MESSAGE);

						} else if (filaTres.size() > filaUm.size()) {
							filaUm.add(filaClientes.peek());
							filaClientes.poll();
							JOptionPane.showMessageDialog(null, filaUm.toArray(), "Fila Caixa 1",
									JOptionPane.INFORMATION_MESSAGE);

						}
					} else if (filaTres.size() > filaUm.size() & filaTres.size() > filaDois.size()) {
						if (filaUm.size() > filaDois.size()) {
							filaDois.add(filaClientes.peek());
							filaClientes.poll();
							JOptionPane.showMessageDialog(null, filaDois.toArray(), "Fila Caixa 2",
									JOptionPane.INFORMATION_MESSAGE);

						} else if (filaDois.size() > filaUm.size()) {
							filaUm.add(filaClientes.peek());
							filaClientes.poll();
							JOptionPane.showMessageDialog(null, filaUm.toArray(), "Fila Caixa 1",
									JOptionPane.INFORMATION_MESSAGE);
						}
					} else if (filaUm.isEmpty() || filaDois.isEmpty() || filaTres.isEmpty()) {

						if (filaUm.isEmpty()) {
							filaUm.add(filaClientes.peek());
							filaClientes.poll();
							JOptionPane.showMessageDialog(null, filaUm.toArray(), "Fila Caixa 1",
									JOptionPane.INFORMATION_MESSAGE);

						} else if (filaDois.isEmpty()) {
							filaDois.add(filaClientes.peek());
							filaClientes.poll();
							JOptionPane.showMessageDialog(null, filaDois.toArray(), "Fila Caixa 2",
									JOptionPane.INFORMATION_MESSAGE);

						} else if (filaTres.isEmpty()) {
							filaTres.add(filaClientes.peek());
							filaClientes.poll();
							JOptionPane.showMessageDialog(null, filaTres.toArray(), "Fila Caixa 3",
									JOptionPane.INFORMATION_MESSAGE);
						}
					}
				}

			} else if (opcao == 3) {
				String escolher = JOptionPane.showInputDialog(null,
						"Escolha um caixa:\n\n 1) Caixa Um\n 2)Caixa Dois\n 3)CaixaTr�s", "Removendo Cliente do Caixa",
						JOptionPane.QUESTION_MESSAGE);
				int num = Integer.parseInt(escolher);
				if (num == 1) {
					if (filaUm.isEmpty()) {
						JOptionPane.showMessageDialog(null, "Caixa 1 Vazio!", "Oops..", JOptionPane.WARNING_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "Obrigada pela prefer�ncia!: " + filaUm.remove(),
								"Removido!", JOptionPane.INFORMATION_MESSAGE);
						// filaUm.remove();
					}
				} else if (num == 2) {
					if (filaDois.isEmpty()) {
						JOptionPane.showMessageDialog(null, "Caixa 2 Vazio!", "Oops...", JOptionPane.WARNING_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "Obrigada pela prefer�ncia!: " + filaDois.remove(),
								"Removido!", JOptionPane.INFORMATION_MESSAGE);
						// filaDois.remove();
					}
				} else if (num == 3) {
					if (filaTres.isEmpty()) {
						JOptionPane.showMessageDialog(null, "Caixa 3 vazio!", "Oops..", JOptionPane.WARNING_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "Obrigada pela prefer�ncia!: " + filaTres.remove(),
								"Removido!", JOptionPane.INFORMATION_MESSAGE);
						// filaTres.remove();
					}
				}
			}
			String continuar = JOptionPane.showInputDialog(null, "Deseja sair?\n\n Insira 2 para SAIR", "Encerrando...",
					JOptionPane.CLOSED_OPTION);
			confirmacao = Integer.parseInt(continuar);
		} while (confirmacao != 2);
	}
}